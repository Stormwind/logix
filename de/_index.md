---
title: Deutsch
---
## card10-Ausgabe: Du kannst ab jetzt weiße Gutscheine gegen card10s beim card10 assembly tauschen.
## Aktuelle Firmware-Version: card10s werden mit dem aktuellen Release "Asparagus" verteilt.
Es ist sehr einfach, deine [card10 firmware zu updaten](/de/firmwareupdate) indem du ein USB-C-Kabel benutzt. Die Update-Anleitung hat auch eine Liste mit existierenden Firmware-Versionen.

# card10

Willkommen im card10 Wiki. Du kannst diese Wikiquelle [browsen](https://git.card10.badge.events.ccc.de/card10/logix) mehr über unsere Gitlab-Instanz [lesen](/de/gitlab).

**card10** ist der Name für das Badge des [Chaos Communication Camp](https://events.ccc.de/camp/2019/wiki/Main_Page). 

### [Los geht's](/de/gettingstarted) 
Du hast gerade dein card10 empfangen. Was jetzt? Die [los geht's](/de/gettingstarted) Seite hat ein bisschen Hilfe wie du das card10 zusammen baust und benutzt.

### [Erste Interhacktionen](/de/firstinterhacktions)
Nachdem du ein bisschen mit card10 herumgespielt hast, willst du lernen, wie du deine eigene Software schreiben kannst. Auch wenn du noch nie programmiert hast, ist es super einfach, ein paar LEDs auf card10 zum blinken zu bringen :)

### [tl;dr](/de/tldr)
Erspar mir die Details, gib mir einfach die Links zur Doku!

## Übersicht

[![Bild des card10 badge](media/ECKyISOXsAYDJZl.jpg)](media/ECKyISOXsAYDJZl.jpg)
>
> Hallo, mein Name ist card10

### Community
  - [IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat): [`freenode.net#card10badge`](ircs://chat.freenode.net:6697/card10badge)
  - oder [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)) (mirror): [`#card10badge:asra.gr`](https://matrix.to/#/#card10badge:asra.gr)
  - [GitLab: `https://git.card10.badge.events.ccc.de`](https://git.card10.badge.events.ccc.de/explore/groups/)

### Social Media
  - [`@card10badge@chaos.social`](https://chaos.social/@card10badge)
  - [`twitter.com/card10badge`](https://twitter.com/card10badge)

### [FAQ](/de/faq)
<!--- Bitte auch zu de/events.md hinzufügen-->

### Events
  - Dauernd:
     - **card10 reparieren/löten/firmware/etc.**: card10 village (R8):
       - Firmware-Arbeit: Bitte bring deinen Laptop mit einem Klon des Firmware Git-Repos und der [toolchain](firmware.card10.badge.events.ccc.de/how-to-build.html)

