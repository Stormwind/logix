---
title: Exposure Notifications
---

COVID-19 Exposure Notifications are based on Bluetooth Low Energy (BLE). They are based on BLE advertisements and can be received (and in general also be sent) by the card10.

> :warning: The Exposure Notification support of the card10 is not meant as an replacement for official Exposure Notification apps. It is merely meant as a tool for the curious.

[![Exposure Notification Stats app](/media/card10-exnostat.jpg)](/media/card10-exnostat.jpg)

The currently released firmware for the card10 does not allow receiving of BLE advertisements.
[Here](https://git.card10.badge.events.ccc.de/card10/firmware/-/merge_requests/392)'s
a Merge Request which implements this functionality. If you have a working toolchain for the card10,
please try and build this MR, as you will have to most up-to-date version.

If you do not have a working toolchain, you can download a [pre-build image](/release/card10-exno-r1.zip).
Simply unzip the archive and merge it onto your card10. See the [firmware update page](/en/firmwareupdate) for details. Most importantly
always use the "safely eject" functionality of your operating system after writing files to the card10.

To use the BLE features of the card10 make sure to enable BLE in the `Bluetooth` app.

> :warning: This firmware has to be considered untested and therefore faulty. Please make a backup of your file system before putting the new firmware onto your card10.

## Exposure Notification Statistics App

After installing the firmware update, you will find a new app on your card10: `Exposure Notification Stats`. If you launch this app (Bluetooth has to be enabled. See above.) you will start to see some statistics about Exposure Notifications in you vicinity. The app will try to distinguish Apple and Google devices based on a heuristic. This heuristic might fail at any point in time. The app shows the number of unique MACs (aka devices) seen during the last 100 seconds. The seconds counter shows how long ago the oldest considered MAC has been received. A low number means that most of the counted devices are still in you vicinity.

Two buttons allow you to change the LED and vibration motor behavior:

 - Off: Default after launching the app.
 - New MAC: Flash an LED (yellow) or strongly vibrate when a new MAC (aka device) is observed.
 - Each RX: Flash an LED (blue) or weakly vibrate when for each received Notification Exposure.
 - Both: Do both ;)

Almost all logic is written in Python and can be modified by you: [`apps/exnostat/__init__.py`](https://git.card10.badge.events.ccc.de/card10/firmware/-/blob/schneider/covid-tracing/preload/apps/exnostat/__init__.py)

### Possible Improvements
 - Write received Exposure Notifications to a file:
   - Try to follow [Google's guidlines](https://developers.google.com/android/exposure-notifications/ble-attenuation-overview#aggregation_over_a_four-second_scan_scaninstance_min_and_average).
   - Try to limit the amout of writes to the flash. Accumulate data in RAM first and write in batches.
 - Disable the screen while not looking at it:
   - The [G-watch app](https://badge.team/projects/g_watch) can serve as a starting point. It uses the gyroscope and accelerometer to detect glances.
 - Show more statistics over time:
   - A graph of accumulated activity over time.
   - Showing RSSI on the front LEDs (will consume more power).
 - Send exposure notifications:
   - Needs changes to epicardium and some crypto efforts.

### Media
[Video of the Exposure Notification Stats app running](/media/card10-exnostat.mp4)


## Resources

 - https://github.com/kauzu/CoronaAppDetector
 - https://github.com/frankrieger/CoronaAppDetectorM5StickC
 - https://github.com/znuh/microbit-corona-scanner
 - https://www.bunniestudios.com/blog/?p=5820


