---
title: Card10 at rC3
---

## Brainstorming
Instead of the regular 37C3, we will have the first ever remote congress end of this year, which is dubbed "rC3"

How can card10 interact with a remote and distributed chaos event?
How can card10 connect to other card10s anywhere on the globe?
What services could it provide, what services could it make use of?

### PTT or mute/unmute input device 
Connect to the local PC via bluetooth LE, display via RGB LEDs the (un)mute and (un)deafen status of 
 - the OS communications device
 - the application that uses the communications device
    - there might be multiple and changing applications, especially for speakers, heralds, VOC and other angels (e.g. mumble, BBB, OBS, discord, teamspeak etc...)
    - different OS's will handle audio differently. Audio routing is a pain. Reliable indication of the current state would improve the situation a lot

### virtual applause
 - detect if the wearer is clapping their hands via one of the accelerometers
 - push this info to a central server
 - somehow show in an overlay in the live stream or via an indicator next to the streaming window how many card10 wearers are clapping in real time


### Tools for speakers, heralds, VOC angels 
    Timing and handing over who is speaking is challenging to coordinate due to the latency of video conference tools. can card10 help here by e.g.
    Notifications who is 
        - ready to talk
        - waiting to talk
        - wanting to ask a question from the audience and waiting for the speaker to finish their sentence
        - having a problem and needs help from the team
        - presentation timer

    Providing real time feedback from the audience to the speaker
        - showing on the LED bargraph how many people are applauding virtually (see above)
        - pulsing the vibration alarm with increasing intensity the more people are clapping
    
    Pushing info about the current state, like the typical online status or the state app at camp (yes, that is potentially sensitive information that the wearer should be able to have control over who is allowed to see it, like with basically any messenging app)
        - online and available
        - online but afk (taking a break, eating,...)
        - not online (connection problems, blackout, sleeping,...)

### Pager
    display short texts on the LCD that can be sent via
        - the compantion app
        - via the POC? Possible to subscribe the compantion app to the EVPN, registering an extension that can receive text messages or can be paged?
        - IRC, matrix, twitter, rocket chat, whatever 
        - display the sender and if they want to be called via DECT/SIP asap/soonish/when convenient/...


### Fahrplan interaction

#### Show on the display which segments are up next
    probably best if requested by presisng a button
    pull the info required via an API from the fahrplan app that needs to be installed on the same phone as the card10 companion app

#### Notification when a favourite segment is about to start
     The fahrplan app allows flagging talks as favourites. 
    Pull those flags and notify the wearer by a configurable alarm (e.g. pulse the vibration alarm, flash LEDs, show the name of the upcoming talk on the display for a few seconds or until the alarm is dismissed by pressing a button)

#### Notification for self organized sessions
    same for the database that contains the SOS's (afaik that calendar has been in the congress wiki until now)
