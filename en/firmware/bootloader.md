---
title: Bootloader
---

# `card10` Bootloader

The bootloader allows you to:

 - Change the software that the `card10` is executing
 - Rescue a `card10` that does not have working software anymore


The bootloader can be activated by pressing the lower right button while turning on the `card10`:

<insert picture here>


Afterwards your operating system will show a new device attached via USB. You can now write and modify files which are on your `card10`. Simply copy all files from a firmware release to the file system and your `card10` is good to go. Simply turn off the `card10` using the button on the top left and turn it back on.

IMPORTANT: Make sure to use your operating "Eject Drive" feature before unplugging or turning off your `card10` (no, `umount` and `sync` are not enough!). This is a know limitation at the moment.

## Updating the Bootloadder

The bootloader can be updated without a debugger. To be documented.

## Inner Workings

The bootloader tries to mount the external flash chip (8 MB) as a FAT file system and then read the `card10.bin` file from it. If the file can be read and is not corrupted, the bootloader checks if the internal flash already contains this file and writes it to the internal flash if needed. Afterwards the bootloader tries to execute the file inside the internal flash.

 - It is not possible to read the file from the internal flash using the bootloader
 - If the bootloader fails to read a `card10.bin` file from the external flash, it will try to boot the file inside the internal flash.


## Trouble Shooting
If operating does not show a file system after activating the bootloader and attaching it via USB, a few things might be at fault:

 - The file system might be corrupted. Please try to create a new FAT file system and copy over the data from a working firmware release.
 - The USB cable might be broken. Check if your computer recognized a new device
 - The `card10` might still be off. Make sure it is actually on. This can be quite hard as the bootloader does not show anything at the moment.


## Inner Inner Workings

The bootloader sits at the beginning of the internal flash. It starts at address `0x1000 0000`. After the ROM code is done initializing the chip, it transfers execution to the code which address `0x1000 0004` points to. The bootloader can have a maximum size of 64 kB (as defined in `max32665_boot.ld`). At address `0x1001 0000` the code for core 0 starts (as defined in `max32665_core0.ld`. This is the address where the bootloader starts to write the contents of `card10.bin`. It then jumps to the address which `0x1001 0004` points to. It takes care to initialize the stack pointer and `SCB->VTOR` before doing so.

Please make sure to rename the `card10.bin` file to something else if you do not want the bootloader to overwrite the contents of the internal flash. This is important if you use GDB to flash your own experimental code. All code gets linked to address `0x1001 0000` (if compiled for core 0) and needs the bootloader to boot into it. This can only work if the bootloader is not able to fine a valid image on the external flash.

The bootloader checks the CRC of `card10.bin`. It needs to be 0. The script `crc_patch.py` in the `bootloader` directory is used to pad a binary file so that its CRC is 0.


### Multi image support
At the moment there is no support for multiple images to be flashed to different offsets. The single `card10.bin` files needs to contain all needed images and the necessary padding between them. This means that to support core 0 as well as core 1 with fixed offsets, padding bytes with the value `0xff` (to save the flash from write cycles) needs to added.

## Known Issues

Please contribute by improving the bootloader:

 - Does properly work with the `sync` or `umount` commands on the host. The host need to `eject` the drive
 - Does not ask the user when it is about to replace the contents of the internal flash
