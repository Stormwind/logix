---
title: Distribution of Prototypes
---

**Hello fellow researcher,**

you want to become part of the card10 research team and help us find out how it will have improved everyone's experience at Chaos Communication Camp 2019?
Here's what you have to know:

* We have around 20 prototypes which we are trying to share with as many interested card10logists as possible. And there are many people interested in working with it.
* There's documentation in our [wiki](https://card10.badge.events.ccc.de/)
* We're trying to find out how the badge will have interacted (?) and used BLE to interface with other installations at camp the camp surroundings
* Cities with hackspaces can get a prototype if there's a volunteer for being an ambassador who takes care of the prototype (trying to avoid too much idle time)

## Cities that already have ambassadors

| City      | Hackerspace                                  | Ambassador  |
|-----------|----------------------------------------------|-------------|
| Amsterdam | [badge.team](https://badge.team)             | the_JinX    |
| Bamberg   | [backspace](https://hackerspace-bamberg.de/) | schinken    |
| Berlin    | [xhain](https://x-hain.de)                   | miriamino   |
| Bremen    | [ccc-hb](https://ccchb.de)                   | lilafisch   |
| Dresden   | [c3d2](https://c3d2.de/space.html)           | Astro       |
| Karlsruhe | [Entropia](https://entropia.de/)             | uberardy    |
| Mannheim  | [RaumZeitLabor](https://raumzeitlabor.de/)   | cheatha     |
| Munich    | [µc3](https://muc.ccc.de)                    | schneider__ |

Most ambassadors can be reached via the #card10badge Freenode/Matrix channel.


### Here's what you can do:
* Think about how you want to contribute/what you want to do with the prototype
* Join our [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)):( [`#card10badge:asra.gr`](https://matrix.to/#/#card10badge:asra.gr))/[IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) ( [`freenode.net#card10badge`](ircs://chat.freenode.net:6697/card10badge))
channel and tell us what you want to do
* Talk to your city's ambassador
* Become an ambassador yourself
* We'll try to find a timeslot for you to work with the prototype
* We send it to you and after the time we agreed upon you send it back to us/the next person


### How to be a great card10logy department ambassador:
* Get more people involved to help with 'ambassadoring' ;P
* Invite others to chat about the card10
* Organise meetups to play with card10
* Invite related groups (wagenburg, fab labs, ...)
* Contact and encourage people who might have great ideas but are not aware of the project/too shy to ask
* Share your research in the card10logix wiki, matrix/irc and @card10badge
